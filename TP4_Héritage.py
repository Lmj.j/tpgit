#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 13:50:42 2020

@author: j19007109
"""
class Point: 
    def __init__(self,x,y):
        self.x=x
        self.y=y
        
    def x(self):
        return (self.x)
    
    def y(self):
        return(self.y)
        
    def __str__(self):
        return ('(' + str(self.x) + ',' + str(self.y) + ')')


###copmposition ?
class Polygone():
    def __init__(self,sommets):
        self.__sommets=sommets
        
    def getSommets(self,i):
        return (self.__sommets[i])
    
    def aire(self):
        Som=0
        for i in range(len(self.sommets)-1):    
            Som = Som + (self.sommets[i].x() + self.sommets[i+1].x()) * (self.sommets[i].y() + self.sommets[i+1].y())
            Som= 0.5*Som
        return Som
    
    def __str__(self):
        for i in range(len(self.sommets)):
            stri= '[' + str(self.sommets[i])+ ']'
        return stri
    


class Triangle(Polygone):
    def __init__(self,sommets,a,b,c):
        super().__init__(sommets)
        self.a=a
        self.b=b
        self.c=c

class Rectangle(Polygone):
    def __init__(self, sommets, xMin, xMax, yMin, yMax):
        super().__init__(sommets)
        self.xMin=xMin
        self.xMax=xMax
        self.yMin=yMin
        self.yMax=yMax
        
class PolygoneRegulier(Polygone):
    def __init__(self, sommets, centre, rayon, nombreSommets):
        super().__init__(sommets)
        for i in range(nombreSommets):
            x1 = centre.x() + rayon * math.cos(2*math.pi*i/nombreSommets)
            y1 = centre.y() + rayon * math.sin(2*math.pi*i/nombreSommets)
            self.sommets.append(Point(x1,y1))

        
A = Point(3,5)
B = Point(2,3)
C = Point(9,2)
D = Point(3,4)   
E = Point(5,7) 
test = Polygone([A,B,C,D,E])
print(test.getSommets(2))
print(test.aire())
print(PolygoneRegulier(Point(1,7),(5,8))
print(Triangle(A,B,C))
print(Rectangle(A,B,C,D))


    